
#include "LHEF3.cc"

using namespace std;

// Example program: Read events from LHEF, recluster partons, print.
int main( int argc, char* argv[] ){

  // Get command-line arguments
  vector<string> arguments;
  for (int i = 0; i < argc; ++i) { 
    arguments.push_back(string(argv[i]));
  }

  // Parse command-line arguments
  // input file
  vector<string>::iterator it
   = std::find(arguments.begin(),arguments.end(),"--output");
  string output = (it != arguments.end()) ? *(it+1) : "";

  // Read input files.
  vector<string> input_file;
  for (int i = 0; i < int(arguments.size()); ++i)
    if (arguments[i] == "--input" && i+1 <= int(arguments.size())-1)
      input_file.push_back(arguments[i+1]);

  vector<LHEF::Reader*> reader;
  for (int i = 0; i < int(input_file.size()); ++i){
    reader.push_back( new LHEF::Reader(input_file[i]));
  }

  // Output lhe file name
  std::ofstream outfile;
  std::string file = output + ".lhef";
  const char* cstring = file.c_str();
  outfile.open(cstring);

  // File writer.
  LHEF::Writer writer(outfile);

  long neve = 0;

  // Get init information.
  writer.version = reader.front()->version;
  writer.headerBlock() << reader.front()->headerComments;
  writer.headerBlock() << reader.front()->headerComments;
  writer.initComments() << reader.front()->initComments;
  writer.initComments() << reader.front()->initComments;
  writer.heprup = reader.front()->heprup;

  cout << "start estimating xsection" << endl;

  // Loop through files to get cross section and number of events.
  double sumwt = 0.;
  double avgwt = 0.;
  int nev=0;
  for (int i = 0; i < int(reader.size()); ++i) {
    double sumwtnow=0.;
    int nevnow=0;
    while ( reader[i]->readEvent() ) {
      ++nevnow;
      sumwtnow += reader[i]->hepeup.XWGTUP;
    }
    cout << "ran through file " << i << " of " << reader.size() << " files: " << input_file[i] 
         << " " << sumwtnow << " " << nevnow << endl;
    avgwt += sumwtnow/nevnow;
    sumwtnow *= nevnow;
    nev+=nevnow;
    sumwt+=sumwtnow;
  }

  avgwt /= (double) reader.size();
  double unitwt = avgwt;

  for (int i = 0; i < int(input_file.size()); ++i) {
    delete reader[i];
    reader[i] = 0;
  }
  cout << "xsection " << sumwt << " " << nev << " " << unitwt << endl;

  vector<LHEF::Reader*> reader2;
  for (int i = 0; i < int(input_file.size()); ++i)
    reader2.push_back( new LHEF::Reader(input_file[i]));

  // Recompute cross section.
  double xstot = avgwt;
  writer.heprup.XSECUP.front() = xstot;
  // Store as weighted events.
  writer.heprup.IDWTUP = -4;

  // Write header and init tags.
  writer.init();

  // Loop through events
  for (int i = 0; i < int(reader2.size()); ++i) {
    cout << "write events from file " << i << endl;
    while ( reader2[i]->readEvent()) {
      writer.eventComments() << reader2[i]->eventComments;
      writer.hepeup = reader2[i]->hepeup;
      //writer.hepeup.XWGTUP = unitwt;
      writer.hepeup.XWGTUP = reader2[i]->hepeup.XWGTUP;
      writer.writeEvent();
    }
  }

  // File end.
  writer.print_end_tag();
  outfile.close();

  // Example done.
  return 0;

}
