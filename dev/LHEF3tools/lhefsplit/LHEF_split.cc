
#include "LHEF3.cc"

// Example program: Read events from LHEF, recluster partons, print.
int main( int argc, char* argv[] ){

  // Input file name.
  std::string infileName  = std::string(argv[1]);

  int nFiles = std::atoi(argv[3]);

  std::cout << "Split " << infileName << " into " << nFiles << " files" << std::endl; 

  std::ifstream infile(infileName.c_str());
  int nEvents = std::atoi(argv[4]);
  if (nEvents < 1) {
    nEvents=0;
    std::string line;
    while(std::getline(infile, line))
      if ( line.find("<event", 0) != std::string::npos
        && line.find("<eventgroup", 0) == std::string::npos)
        nEvents++;
  }
  int iFile = 0;
  int nEventsPerFile = nEvents/nFiles;

  // Output lhe file name
  std::string outfileName = std::string(argv[2]);
  std::vector<std::ofstream*> outfile;
  for (int i=1; i <= nFiles; ++i) {
    outfile.push_back(new std::ofstream());
    std::stringstream in;
    in << "_" << i;
    std::string file = outfileName + in.str() + ".lhef";
    const char* cstring = file.c_str();
    outfile.back()->open(cstring);
  }

  // File reader.
  LHEF::Reader reader(infileName);

  // File writer.
  std::vector <LHEF::Writer*> writer;
  for (unsigned int i=0; i < outfile.size(); ++i) writer.push_back( new LHEF::Writer((*outfile[i])) );

  long neve = 0;

  std::cout << "Write " <<  nEventsPerFile << " to each file." << std::endl; 

  // Get init information.
  for (unsigned int i=0; i < writer.size(); ++i) {
    writer[i]->version = reader.version;
    writer[i]->headerBlock() << reader.headerComments;
    writer[i]->initComments() << reader.initComments;
    writer[i]->heprup = reader.heprup;
    writer[i]->init();
  }

  // Read each event and write them out again, also in reclustered form. 
  std::cout << "Start writing file #" << iFile << std::endl;
  while ( reader.readEvent() ) {
    ++neve;
    /*bool goodfile = true;
    if (neve%nEventsPerFile == 1 && neve != 1) {
      // Keep writing until all evnt groups are closed.
      while (reader.hasOpenEventgroup() ) {
        std::cout << "continue writing events until event group is closed." << std::endl;
        if (!reader.readEvent()) {
          goodfile = false;
          break;
        }
        std::string outside=reader.outsideBlock;
        if (!outside.empty()) {
          if (outside[outside.length()-1] == '\n')
            outside.erase(outside.length()-1);
          *outfile[iFile] << outside << std::endl;
        }
        writer[iFile]->eventComments() << reader.eventComments;
        writer[iFile]->hepeup = reader.hepeup;

        std::cout << __LINE__ << std::endl;

        writer[iFile]->writeEvent();
      }
      writer[iFile]->print_end_tag();
      ++iFile;
      std::cout << "Start writing file #" << iFile << std::endl;
    }*/

    // Write event.
    std::string outside=reader.outsideBlock;
    if (!outside.empty()) {
      if (outside[outside.length()-1] == '\n')
        outside.erase(outside.length()-1);
      *outfile[iFile] << outside << std::endl;
    }
    writer[iFile]->eventComments() << reader.eventComments;
    writer[iFile]->hepeup = reader.hepeup;
    writer[iFile]->writeEvent();

    if (neve%nEventsPerFile == 1 && neve != 1) {
      // Keep writing until all evnt groups are closed.
      bool printEventgroupEnd=false;
      while (reader.hasOpenEventgroup() ) {
        printEventgroupEnd=true;
        if (!reader.readEvent()) break;
        std::string outside=reader.outsideBlock;
        if (!outside.empty()) {
          if (outside[outside.length()-1] == '\n')
            outside.erase(outside.length()-1);
          *outfile[iFile] << outside << std::endl;
        }
        writer[iFile]->eventComments() << reader.eventComments;
        writer[iFile]->hepeup = reader.hepeup;
        writer[iFile]->writeEvent();
      }

      // Let reader skip a line
      if (reader.hasEventgroup()) {
        reader.skipLine();
        writer[iFile]->print("</eventgroup>\n");
      }
      writer[iFile]->print_end_tag();
      if ( iFile < writer.size()-1 ) {
        ++iFile;
        std::cout << "Start writing file #" << iFile << " of " << writer.size() << " after " << neve << " events " << std::endl;
      }
    }

  }

  // Last file end.
  // Let reader skip a line
  if (reader.hasEventgroup()) {
    writer[iFile]->print("</eventgroup>\n");
  }
  writer[iFile]->print_end_tag();

  for (unsigned int i=0; i < outfile.size(); ++i) outfile[i]->close();
  for (unsigned int i=0; i < outfile.size(); ++i) delete outfile[i];
  for (unsigned int i=0; i < writer.size(); ++i) delete writer[i];

  // Example done.
  return 0;

}
