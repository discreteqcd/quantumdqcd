g++ -g -o split.exe LHEF_split.cc

njet=$1

# split exclusive file
file=/scratch/kingma/prestel/LHEF3tools/lhefcombine/n3lo-dy${njet}p-exclusive-sample.lhef
nevinfile=-1
out=n3lo-dy${njet}p-exclusive-sample
dir=dy${njet}p-exclusive
nsplit=1000
mkdir -p $dir
./split.exe $file $dir/$out $nsplit $nevinfile

