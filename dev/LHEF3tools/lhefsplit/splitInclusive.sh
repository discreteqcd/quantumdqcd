g++ -g -o split.exe LHEF_split.cc

njet=$1

# split processed inclusive 3j file
file=/scratch/kingma/prestel/LHEF3tools/lhefcombine/n3lo-dy${njet}p-inclusive-sample.lhef
nevinfile=-1
out=n3lo-dy${njet}p-inclusive-sample
dir=dy${njet}p-inclusive
nsplit=1000
mkdir -p $dir
./split.exe $file $dir/$out $nsplit $nevinfile

