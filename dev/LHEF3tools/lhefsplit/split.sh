g++ -g -o split.exe LHEF_split.cc

# split *unprocessed* inclusive 3j file
file=/scratch/kingma/prestel/LHEF3tools/lhefcombine/n3lo-dy3p-input-sample.lhef
nevinfile=15000000
out=n3lo-dy3p-input-sample
dir=dy3p-input
nsplit=1000
mkdir -p $dir
./split.exe $file $dir/$out $nsplit $nevinfile

# split processed inclusive 3j file
#file=/scratch/kingma/prestel/LHEF3tools/lhefcombine/dygnew-1000.lhef
#nevinfile=1000000
#out=n3lo-dy3p-inclusive-sample
#dir=dy3p-inc
#nsplit=1000
#mkdir -p $dir
#./split.exe $file $dir/$out $nsplit $nevinfile

# split processed inclusive 2j file
#file=/scratch/atwood/prestel/work/2019/DIREblub2/DIRE-2.004yy/n3lo-dy2p-inclusive-sample.lhe
#nevinfile=1000000
#out=n3lo-dy2p-inclusive-sample
#dir=dy2p-inc
#nsplit=1000
#mkdir -p $dir
#./split.exe $file $dir/$out $nsplit $nevinfile

# split *unprocessed* inclusive 2j file
#file=/scratch/kingma/prestel/LHEF3tools/dy2gnew3.lhef
#nevinfile=15000000
#out=n3lo-dy2p-input-sample
#dir=dy2p-input
#nsplit=1000
#mkdir -p $dir
#./split.exe $file $dir/$out $nsplit $nevinfile

# split processed inclusive 1j file
#echo "split processed inclusive 1j file"
#file=/scratch/atwood/prestel/work/2019/DIREblub2/DIRE-2.004yy/n3lo-dy1p-inclusive-sample.lhe
#nevinfile=1000000
#out=n3lo-dy1p-inclusive-sample
#dir=dy1p-inc
#nsplit=1000
#mkdir -p $dir
#./split.exe $file $dir/$out $nsplit $nevinfile

# split processed inclusive 0j file
#echo "split processed inclusive 0j file"
#file=/scratch/atwood/prestel/work/2019/DIREblub2/DIRE-2.004yy/n3lo-dy0p-inclusive-sample.lhe
#nevinfile=1000000
#out=n3lo-dy0p-inclusive-sample
#dir=dy0p-inc
#nsplit=1000
#mkdir -p $dir
#./split.exe $file $dir/$out $nsplit $nevinfile




# split exclusive 2j file
echo "split exclusive 2j file"
file=/scratch/kingma/prestel/LHEF3tools/lhefcombine/n3lo-dy2p-exclusive-sample-full1000.lhef
nevinfile=2000000
out=n3lo-dy2p-exclusive-sample
dir=dy2p-exc
nsplit=1000
mkdir -p $dir
./split.exe $file $dir/$out $nsplit $nevinfile


# split exclusive 1j file
#echo "split exclusive 1j file"
#file=/scratch/atwood/prestel/work/2019/DIREblub2/DIRE-2.004yy/n3lo-dy1p-exclusive-sample.lhe
#nevinfile=2000000
#out=n3lo-dy1p-exclusive-sample
#dir=dy1p-exc
#nsplit=1000
#mkdir -p $dir
#./split.exe $file $dir/$out $nsplit $nevinfile

# split exclusive 0j file
#echo "split exclusive 0j file"
#file=/scratch/atwood/prestel/work/2019/DIREblub2/DIRE-2.004yy/n3lo-dy0p-exclusive-sample.lhe
#nevinfile=2000000
#out=n3lo-dy0p-exclusive-sample
#dir=dy0p-exc
#nsplit=1000
#mkdir -p $dir
#./split.exe $file $dir/$out $nsplit $nevinfile
