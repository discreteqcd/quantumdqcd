g++ -g -o split.exe LHEF_split.cc
njet=$1
file=/scratch/kingma/prestel/LHEF3tools/lhefcombine/n3lo-dy${njet}p-input-sample.lhef
nevinfile=15000000
out=n3lo-dy${njet}p-input-sample
dir=dy${njet}p-input
nsplit=1000
mkdir -p $dir
./split.exe $file $dir/$out $nsplit $nevinfile
