# Quantum Discrete QCD: a quantum implementation of discretized particle evolution

Welcome to the repository of the Quantum DQCD parton shower introduced in [this publication](https://arxiv.org/abs/2207.10694). This implementation is based on generating and caching the "groves" of the DQCD upon initialization, and sampling the "grove" using quantum walks.

## Requirements 

The code is written in Python 3 and Qiskit.

## Running the code.

Check out the Jupyer notebooks in the quantum_dqcd directory.

## Authors and acknowledgment

The ideas for the algorithm were developed by Simon Williams and Stefan -- feel free to get in touch! 

## License

This software is licenced under the GNU GPL v2 or later.

