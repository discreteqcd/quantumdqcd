
smearingfactor=10

def isclose(a, b, rel_tol=1e-09, abs_tol=0.0):
  return abs(a-b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)

def eq (a, b, rel_tol=1e-06, abs_tol=1e-10):
  return isclose(a,b, rel_tol, abs_tol)

def ne (a, b, rel_tol=1e-06, abs_tol=1e-10):
  return not isclose(a,b, rel_tol, abs_tol)

def lt (a, b):
  if isclose(a,b,1e-06,1e-10):
    return False
  return (a-b<0.0)

def gt (a, b):
  if isclose(a,b,1e-06,1e-10):
    return False
  return (a-b>0.0)

def le (a, b):
  return eq(a,b) or lt(a,b)

def ge (a, b):
  return  eq(a,b) or gt(a,b)

