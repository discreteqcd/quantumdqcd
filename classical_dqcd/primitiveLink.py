
import math as m
import sys

from qcd import deltaYg, deltaYq

class PrimitiveLink:

    def __init__(self, lineColor, startIndex, stopIndex, startIsGluon, stopIsGluon, bkgrnd_ybin = 0.):
        # basic attributes
        self.length = deltaYg
        self.lineColor = lineColor
        self.startIndex = startIndex
        self.stopIndex = stopIndex
        self.startIsGluon = startIsGluon
        self.stopIsGluon = stopIsGluon
        self.bkgrnd_ybin = bkgrnd_ybin
        # attributes that are not known at construction time
        self.particle_pos = 0
        self.y = 0.0
        self.kappa = 0.0
        self.y_offset = 0.0
        self.kappa_offset = 0.0
        self.kappaMax = -1.
        self.kappaMax_save = -1.
        self.startIsGluon_save = self.startIsGluon
        self.stopIsGluon_save = self.stopIsGluon
        self.particle_pos_save = self.particle_pos
        return None

    def list (self):
        print ("lineColor=",self.lineColor, "start=",self.startIndex, "stop=",self.stopIndex, "startGluon=",self.startIsGluon, "stopGluon=",self.stopIsGluon, " particle=", self.particle_pos, " length", self.real_length(), " kappa ", self.kappa, self.real_kappa())
        return None

    def store_particle_pos(self, np):
        self.particle_pos=np
        self.particle_pos_save=np
        return None

    def store_kappa_max(self, k):
        self.kappaMax=k
        self.kappaMax_save=k
        return None

    def reset(self):
        self.y_offset=0.0
        self.kappa_offset=0.0
        self.kappaMax = self.kappaMax_save
        self.startIsGluon = self.startIsGluon_save
        self.stopIsGluon = self.stopIsGluon_save
        pos = self.particle_pos
        self.particle_pos = self.particle_pos_save
        return None

    def real_length(self):
        return self.length + self.y_offset + 0.5*self.kappa_offset

    def real_kappa(self):
        return self.kappa + self.kappa_offset

    def real_y(self):
        return self.y + self.y_offset
