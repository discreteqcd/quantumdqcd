
#include "Pythia8/Pythia.h"
#include "Pythia8Plugins/HepMC2.h"
using namespace Pythia8;

//==========================================================================

int main(  int, char* argv[] ) {
  // Generator
  Pythia pythia;
  // Load configuration file
  pythia.readFile("hadronize.cmnd");
  ostringstream file;
  file << argv[1];
  pythia.readString("Beams:LHEF = " + file.str());
  // Interface for conversion from Pythia8::Event to HepMC one.
  Pythia8ToHepMC toHepMC(argv[2]);
  int nEvent      = pythia.settings.mode("Main:numberOfEvents");
  // Initialise and list settings
  pythia.init();
  // Begin event loop; generate until nEvent events are processed
  // or end of LHEF file
  int iEvent = 0;
  while (true) {
    // Generate the next event
    if (!pythia.next()) {
      // If failure because reached end of file then exit event loop
      if (pythia.info.atEndOfFile()) break;
      // Otherwise count event failure and continue/exit as necessary
      //cout << "Warning: event " << iEvent << " failed" << endl;
      continue;
    }
    // If nEvent is set, check and exit loop if necessary
    ++iEvent;
    if (nEvent != 0 && iEvent == nEvent) break;
    toHepMC.writeNextEvent( pythia );
  } // End of event loop.

  // Statistics, histograms and veto information
  pythia.stat();
  return 0;
}
